
	parameter [ 0:0] ENABLE_COUNTERS = 0,
	parameter [ 0:0] ENABLE_COUNTERS64 = 1,
	parameter [ 0:0] ENABLE_REGS_16_31 = 1,
	parameter [ 0:0] ENABLE_REGS_DUALPORT = 1,
	parameter [ 0:0] LATCHED_MEM_RDATA = 1,
	parameter [ 0:0] TWO_STAGE_SHIFT = 0,
	parameter [ 0:0] BARREL_SHIFTER = 0,
	parameter [ 0:0] TWO_CYCLE_COMPARE = 0,
	parameter [ 0:0] TWO_CYCLE_ALU = 1,
	parameter [ 0:0] COMPRESSED_ISA = 0,
	parameter [ 0:0] CATCH_MISALIGN = 0,
	parameter [ 0:0] CATCH_ILLINSN = 0,
	parameter [ 0:0] ENABLE_PCPI = 0,
	parameter [ 0:0] ENABLE_MUL = 0,
	parameter [ 0:0] ENABLE_FAST_MUL = 0,
	parameter [ 0:0] ENABLE_DIV = 0,
	parameter [ 0:0] ENABLE_IRQ = 0,
	parameter [ 0:0] ENABLE_IRQ_QREGS = 1,
	parameter [ 0:0] ENABLE_IRQ_TIMER = 1,
	parameter [ 0:0] ENABLE_TRACE = 0,
	parameter [ 0:0] REGS_INIT_ZERO = 0,
	parameter [31:0] MASKED_IRQ = 32'h 0000_0000,
	parameter [31:0] LATCHED_IRQ = 32'h ffff_ffff,
	parameter [31:0] PROGADDR_RESET = 32'h 0000_0000,
	parameter [31:0] PROGADDR_IRQ = 32'h 0000_0010,
	parameter [31:0] STACKADDR = 32'h ffff_ffff



	always @(posedge clk) begin
		trap <= 0;
		reg_sh <= 'bx;
		reg_out <= 'bx;
		set_mem_do_rinst = 0;
		set_mem_do_rdata = 0;
		set_mem_do_wdata = 0;

		alu_out_0_q <= alu_out_0;
		alu_out_q <= alu_out;

		alu_wait <= 0;
		alu_wait_2 <= 0;

		if (launch_next_insn) begin
			dbg_rs1val <= 'bx;
			dbg_rs2val <= 'bx;
			dbg_rs1val_valid <= 0;
			dbg_rs2val_valid <= 0;
		end

		if (WITH_PCPI && CATCH_ILLINSN) begin
			if (resetn && pcpi_valid && !pcpi_int_wait) begin
				if (pcpi_timeout_counter)
					pcpi_timeout_counter <= pcpi_timeout_counter - 1;
			end else
				pcpi_timeout_counter <= ~0;
			pcpi_timeout <= !pcpi_timeout_counter;
		end

		if (ENABLE_COUNTERS) begin
			count_cycle <= resetn ? count_cycle + 1 : 0;
			if (!ENABLE_COUNTERS64) count_cycle[63:32] <= 0;
		end else begin
			count_cycle <= 'bx;
			count_instr <= 'bx;
		end

		next_irq_pending = ENABLE_IRQ ? irq_pending & LATCHED_IRQ : 'bx;

		if (ENABLE_IRQ && ENABLE_IRQ_TIMER && timer) begin
			if (timer - 1 == 0)
				next_irq_pending[irq_timer] = 1;
			timer <= timer - 1;
		end

		if (ENABLE_IRQ) begin
			next_irq_pending = next_irq_pending | irq;
		end

		decoder_trigger <= mem_do_rinst && mem_done;
		decoder_trigger_q <= decoder_trigger;
		decoder_pseudo_trigger <= 0;
		decoder_pseudo_trigger_q <= decoder_pseudo_trigger;
		do_waitirq <= 0;

		trace_valid <= 0;

		if (!ENABLE_TRACE)
			trace_data <= 'bx;

		if (!resetn) begin
			reg_pc <= PROGADDR_RESET;
			reg_next_pc <= PROGADDR_RESET;
			if (ENABLE_COUNTERS)
				count_instr <= 0;
			latched_store <= 0;
			latched_stalu <= 0;
			latched_branch <= 0;
			latched_trace <= 0;
			latched_is_lu <= 0;
			latched_is_lh <= 0;
			latched_is_lb <= 0;
			pcpi_valid <= 0;
			pcpi_timeout <= 0;
			irq_active <= 0;
			irq_delay <= 0;
			irq_mask <= ~0;
			next_irq_pending = 0;
			irq_state <= 0;
			eoi <= 0;
			timer <= 0;
			if (~STACKADDR) begin
				latched_store <= 1;
				latched_rd <= 2;
				reg_out <= STACKADDR;
			end
			cpu_state <= cpu_state_fetch;
		end else
		(* parallel_case, full_case *)
		case (cpu_state)
			cpu_state_trap: begin
				trap <= 1;
			end

			cpu_state_fetch: begin
				mem_do_rinst <= !decoder_trigger && !do_waitirq;
				mem_wordsize <= 0;

				current_pc = reg_next_pc;

				(* parallel_case *)
				case (1'b1)
					latched_branch: begin
						current_pc = latched_store ? (latched_stalu ? alu_out_q : reg_out) & ~1 : reg_next_pc;
					end
					latched_store && !latched_branch: begin
					end
					ENABLE_IRQ && irq_state[0]: begin
						current_pc = PROGADDR_IRQ;
						irq_active <= 1;
						mem_do_rinst <= 1;
					end
					ENABLE_IRQ && irq_state[1]: begin
						eoi <= irq_pending & ~irq_mask;
						next_irq_pending = next_irq_pending & irq_mask;
					end
				endcase

				if (ENABLE_TRACE && latched_trace) begin
					latched_trace <= 0;
					trace_valid <= 1;
					if (latched_branch)
						trace_data <= (irq_active ? TRACE_IRQ : 0) | TRACE_BRANCH | (current_pc & 32'hfffffffe);
					else
						trace_data <= (irq_active ? TRACE_IRQ : 0) | (latched_stalu ? alu_out_q : reg_out);
				end

				reg_pc <= current_pc;
				reg_next_pc <= current_pc;

				latched_store <= 0;
				latched_stalu <= 0;
				latched_branch <= 0;
				latched_is_lu <= 0;
				latched_is_lh <= 0;
				latched_is_lb <= 0;
				latched_rd <= decoded_rd;
				latched_compr <= compressed_instr;

				if (ENABLE_IRQ && ((decoder_trigger && !irq_active && !irq_delay && |(irq_pending & ~irq_mask)) || irq_state)) begin
					irq_state <=
						irq_state == 2'b00 ? 2'b01 :
						irq_state == 2'b01 ? 2'b10 : 2'b00;
					latched_compr <= latched_compr;
					if (ENABLE_IRQ_QREGS)
						latched_rd <= irqregs_offset | irq_state[0];
					else
						latched_rd <= irq_state[0] ? 4 : 3;
				end else
				if (ENABLE_IRQ && (decoder_trigger || do_waitirq) && instr_waitirq) begin
					if (irq_pending) begin
						latched_store <= 1;
						reg_out <= irq_pending;
						reg_next_pc <= current_pc + (compressed_instr ? 2 : 4);
						mem_do_rinst <= 1;
					end else
						do_waitirq <= 1;
				end else
				if (decoder_trigger) begin
					irq_delay <= irq_active;
					reg_next_pc <= current_pc + (compressed_instr ? 2 : 4);
					if (ENABLE_TRACE)
						latched_trace <= 1;
					if (ENABLE_COUNTERS) begin
						count_instr <= count_instr + 1;
						if (!ENABLE_COUNTERS64) count_instr[63:32] <= 0;
					end
					if (instr_jal) begin
						mem_do_rinst <= 1;
						reg_next_pc <= current_pc + decoded_imm_uj;
						latched_branch <= 1;
					end else begin
						mem_do_rinst <= 0;
						mem_do_prefetch <= !instr_jalr && !instr_retirq;
						cpu_state <= cpu_state_ld_rs1;
					end
				end
			end

			cpu_state_ld_rs1: begin
				reg_op1 <= 'bx;
				reg_op2 <= 'bx;

				(* parallel_case *)
				case (1'b1)
					(CATCH_ILLINSN || WITH_PCPI) && instr_trap: begin
						if (WITH_PCPI) begin
							reg_op1 <= cpuregs_rs1;
							dbg_rs1val <= cpuregs_rs1;
							dbg_rs1val_valid <= 1;
							if (ENABLE_REGS_DUALPORT) begin
								pcpi_valid <= 1;
								reg_sh <= cpuregs_rs2;
								reg_op2 <= cpuregs_rs2;
								dbg_rs2val <= cpuregs_rs2;
								dbg_rs2val_valid <= 1;
								if (pcpi_int_ready) begin
									mem_do_rinst <= 1;
									pcpi_valid <= 0;
									reg_out <= pcpi_int_rd;
									latched_store <= pcpi_int_wr;
									cpu_state <= cpu_state_fetch;
								end else
								if (CATCH_ILLINSN && (pcpi_timeout || instr_ecall_ebreak)) begin
									pcpi_valid <= 0;
									if (ENABLE_IRQ && !irq_mask[irq_ebreak] && !irq_active) begin
										next_irq_pending[irq_ebreak] = 1;
										cpu_state <= cpu_state_fetch;
									end else
										cpu_state <= cpu_state_trap;
								end
							end else begin
								cpu_state <= cpu_state_ld_rs2;
							end
						end else begin
							if (ENABLE_IRQ && !irq_mask[irq_ebreak] && !irq_active) begin
								next_irq_pending[irq_ebreak] = 1;
								cpu_state <= cpu_state_fetch;
							end else
								cpu_state <= cpu_state_trap;
						end
					end
					ENABLE_COUNTERS && is_rdcycle_rdcycleh_rdinstr_rdinstrh: begin
						(* parallel_case, full_case *)
						case (1'b1)
							instr_rdcycle:
								reg_out <= count_cycle[31:0];
							instr_rdcycleh && ENABLE_COUNTERS64:
								reg_out <= count_cycle[63:32];
							instr_rdinstr:
								reg_out <= count_instr[31:0];
							instr_rdinstrh && ENABLE_COUNTERS64:
								reg_out <= count_instr[63:32];
						endcase
						latched_store <= 1;
						cpu_state <= cpu_state_fetch;
					end
					is_lui_auipc_jal: begin
						reg_op1 <= instr_lui ? 0 : reg_pc;
						reg_op2 <= decoded_imm;
						if (TWO_CYCLE_ALU)
							alu_wait <= 1;
						else
							mem_do_rinst <= mem_do_prefetch;
						cpu_state <= cpu_state_exec;
					end
					ENABLE_IRQ && ENABLE_IRQ_QREGS && instr_getq: begin
						reg_out <= cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						latched_store <= 1;
						cpu_state <= cpu_state_fetch;
					end
					ENABLE_IRQ && ENABLE_IRQ_QREGS && instr_setq: begin
						reg_out <= cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						latched_rd <= latched_rd | irqregs_offset;
						latched_store <= 1;
						cpu_state <= cpu_state_fetch;
					end
					ENABLE_IRQ && instr_retirq: begin
						eoi <= 0;
						irq_active <= 0;
						latched_branch <= 1;
						latched_store <= 1;
						reg_out <= CATCH_MISALIGN ? (cpuregs_rs1 & 32'h fffffffe) : cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						cpu_state <= cpu_state_fetch;
					end
					ENABLE_IRQ && instr_maskirq: begin
						latched_store <= 1;
						reg_out <= irq_mask;
						irq_mask <= cpuregs_rs1 | MASKED_IRQ;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						cpu_state <= cpu_state_fetch;
					end
					ENABLE_IRQ && ENABLE_IRQ_TIMER && instr_timer: begin
						latched_store <= 1;
						reg_out <= timer;
						timer <= cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						cpu_state <= cpu_state_fetch;
					end
					is_lb_lh_lw_lbu_lhu && !instr_trap: begin
						reg_op1 <= cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						cpu_state <= cpu_state_ldmem;
						mem_do_rinst <= 1;
					end
					is_slli_srli_srai && !BARREL_SHIFTER: begin
						reg_op1 <= cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						reg_sh <= decoded_rs2;
						cpu_state <= cpu_state_shift;
					end
					is_jalr_addi_slti_sltiu_xori_ori_andi, is_slli_srli_srai && BARREL_SHIFTER: begin
						reg_op1 <= cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						reg_op2 <= is_slli_srli_srai && BARREL_SHIFTER ? decoded_rs2 : decoded_imm;
						if (TWO_CYCLE_ALU)
							alu_wait <= 1;
						else
							mem_do_rinst <= mem_do_prefetch;
						cpu_state <= cpu_state_exec;
					end
					default: begin
						reg_op1 <= cpuregs_rs1;
						dbg_rs1val <= cpuregs_rs1;
						dbg_rs1val_valid <= 1;
						if (ENABLE_REGS_DUALPORT) begin
							reg_sh <= cpuregs_rs2;
							reg_op2 <= cpuregs_rs2;
							dbg_rs2val <= cpuregs_rs2;
							dbg_rs2val_valid <= 1;
							(* parallel_case *)
							case (1'b1)
								is_sb_sh_sw: begin
									cpu_state <= cpu_state_stmem;
									mem_do_rinst <= 1;
								end
								is_sll_srl_sra && !BARREL_SHIFTER: begin
									cpu_state <= cpu_state_shift;
								end
								default: begin
									if (TWO_CYCLE_ALU || (TWO_CYCLE_COMPARE && is_beq_bne_blt_bge_bltu_bgeu)) begin
										alu_wait_2 <= TWO_CYCLE_ALU && (TWO_CYCLE_COMPARE && is_beq_bne_blt_bge_bltu_bgeu);
										alu_wait <= 1;
									end else
										mem_do_rinst <= mem_do_prefetch;
									cpu_state <= cpu_state_exec;
								end
							endcase
						end else
							cpu_state <= cpu_state_ld_rs2;
					end
				endcase
			end

			cpu_state_ld_rs2: begin
				reg_sh <= cpuregs_rs2;
				reg_op2 <= cpuregs_rs2;
				dbg_rs2val <= cpuregs_rs2;
				dbg_rs2val_valid <= 1;

				(* parallel_case *)
				case (1'b1)
					WITH_PCPI && instr_trap: begin
						pcpi_valid <= 1;
						if (pcpi_int_ready) begin
							mem_do_rinst <= 1;
							pcpi_valid <= 0;
							reg_out <= pcpi_int_rd;
							latched_store <= pcpi_int_wr;
							cpu_state <= cpu_state_fetch;
						end else
						if (CATCH_ILLINSN && (pcpi_timeout || instr_ecall_ebreak)) begin
							pcpi_valid <= 0;
							if (ENABLE_IRQ && !irq_mask[irq_ebreak] && !irq_active) begin
								next_irq_pending[irq_ebreak] = 1;
								cpu_state <= cpu_state_fetch;
							end else
								cpu_state <= cpu_state_trap;
						end
					end
					is_sb_sh_sw: begin
						cpu_state <= cpu_state_stmem;
						mem_do_rinst <= 1;
					end
					is_sll_srl_sra && !BARREL_SHIFTER: begin
						cpu_state <= cpu_state_shift;
					end
					default: begin
						if (TWO_CYCLE_ALU || (TWO_CYCLE_COMPARE && is_beq_bne_blt_bge_bltu_bgeu)) begin
							alu_wait_2 <= TWO_CYCLE_ALU && (TWO_CYCLE_COMPARE && is_beq_bne_blt_bge_bltu_bgeu);
							alu_wait <= 1;
						end else
							mem_do_rinst <= mem_do_prefetch;
						cpu_state <= cpu_state_exec;
					end
				endcase
			end

			cpu_state_exec: begin
				reg_out <= reg_pc + decoded_imm;
				if ((TWO_CYCLE_ALU || TWO_CYCLE_COMPARE) && (alu_wait || alu_wait_2)) begin
					mem_do_rinst <= mem_do_prefetch && !alu_wait_2;
					alu_wait <= alu_wait_2;
				end else
				if (is_beq_bne_blt_bge_bltu_bgeu) begin
					latched_rd <= 0;
					latched_store <= TWO_CYCLE_COMPARE ? alu_out_0_q : alu_out_0;
					latched_branch <= TWO_CYCLE_COMPARE ? alu_out_0_q : alu_out_0;
					if (mem_done)
						cpu_state <= cpu_state_fetch;
					if (TWO_CYCLE_COMPARE ? alu_out_0_q : alu_out_0) begin
						decoder_trigger <= 0;
						set_mem_do_rinst = 1;
					end
				end else begin
					latched_branch <= instr_jalr;
					latched_store <= 1;
					latched_stalu <= 1;
					cpu_state <= cpu_state_fetch;
				end
			end

			cpu_state_shift: begin
				latched_store <= 1;
				if (reg_sh == 0) begin
					reg_out <= reg_op1;
					mem_do_rinst <= mem_do_prefetch;
					cpu_state <= cpu_state_fetch;
				end else if (TWO_STAGE_SHIFT && reg_sh >= 4) begin
					(* parallel_case, full_case *)
					case (1'b1)
						instr_slli || instr_sll: reg_op1 <= reg_op1 << 4;
						instr_srli || instr_srl: reg_op1 <= reg_op1 >> 4;
						instr_srai || instr_sra: reg_op1 <= $signed(reg_op1) >>> 4;
					endcase
					reg_sh <= reg_sh - 4;
				end else begin
					(* parallel_case, full_case *)
					case (1'b1)
						instr_slli || instr_sll: reg_op1 <= reg_op1 << 1;
						instr_srli || instr_srl: reg_op1 <= reg_op1 >> 1;
						instr_srai || instr_sra: reg_op1 <= $signed(reg_op1) >>> 1;
					endcase
					reg_sh <= reg_sh - 1;
				end
			end

			cpu_state_stmem: begin
				if (ENABLE_TRACE)
					reg_out <= reg_op2;
				if (!mem_do_prefetch || mem_done) begin
					if (!mem_do_wdata) begin
						(* parallel_case, full_case *)
						case (1'b1)
							instr_sb: mem_wordsize <= 2;
							instr_sh: mem_wordsize <= 1;
							instr_sw: mem_wordsize <= 0;
						endcase
						if (ENABLE_TRACE) begin
							trace_valid <= 1;
							trace_data <= (irq_active ? TRACE_IRQ : 0) | TRACE_ADDR | ((reg_op1 + decoded_imm) & 32'hffffffff);
						end
						reg_op1 <= reg_op1 + decoded_imm;
						set_mem_do_wdata = 1;
					end
					if (!mem_do_prefetch && mem_done) begin
						cpu_state <= cpu_state_fetch;
						decoder_trigger <= 1;
						decoder_pseudo_trigger <= 1;
					end
				end
			end

			cpu_state_ldmem: begin
				latched_store <= 1;
				if (!mem_do_prefetch || mem_done) begin
					if (!mem_do_rdata) begin
						(* parallel_case, full_case *)
						case (1'b1)
							instr_lb || instr_lbu: mem_wordsize <= 2;
							instr_lh || instr_lhu: mem_wordsize <= 1;
							instr_lw: mem_wordsize <= 0;
						endcase
						latched_is_lu <= is_lbu_lhu_lw;
						latched_is_lh <= instr_lh;
						latched_is_lb <= instr_lb;
						if (ENABLE_TRACE) begin
							trace_valid <= 1;
							trace_data <= (irq_active ? TRACE_IRQ : 0) | TRACE_ADDR | ((reg_op1 + decoded_imm) & 32'hffffffff);
						end
						reg_op1 <= reg_op1 + decoded_imm;
						set_mem_do_rdata = 1;
					end
					if (!mem_do_prefetch && mem_done) begin
						(* parallel_case, full_case *)
						case (1'b1)
							latched_is_lu: reg_out <= mem_rdata_word;
							latched_is_lh: reg_out <= $signed(mem_rdata_word[15:0]);
							latched_is_lb: reg_out <= $signed(mem_rdata_word[7:0]);
						endcase
						decoder_trigger <= 1;
						decoder_pseudo_trigger <= 1;
						cpu_state <= cpu_state_fetch;
					end
				end
			end
		endcase

		if (CATCH_MISALIGN && resetn && (mem_do_rdata || mem_do_wdata)) begin
			if (mem_wordsize == 0 && reg_op1[1:0] != 0) begin
				if (ENABLE_IRQ && !irq_mask[irq_buserror] && !irq_active) begin
					next_irq_pending[irq_buserror] = 1;
				end else
					cpu_state <= cpu_state_trap;
			end
			if (mem_wordsize == 1 && reg_op1[0] != 0) begin
				if (ENABLE_IRQ && !irq_mask[irq_buserror] && !irq_active) begin
					next_irq_pending[irq_buserror] = 1;
				end else
					cpu_state <= cpu_state_trap;
			end
		end
		if (CATCH_MISALIGN && resetn && mem_do_rinst && (COMPRESSED_ISA ? reg_pc[0] : |reg_pc[1:0])) begin
			if (ENABLE_IRQ && !irq_mask[irq_buserror] && !irq_active) begin
				next_irq_pending[irq_buserror] = 1;
			end else
				cpu_state <= cpu_state_trap;
		end
		if (!CATCH_ILLINSN && decoder_trigger_q && !decoder_pseudo_trigger_q && instr_ecall_ebreak) begin
			cpu_state <= cpu_state_trap;
		end

		if (!resetn || mem_done) begin
			mem_do_prefetch <= 0;
			mem_do_rinst <= 0;
			mem_do_rdata <= 0;
			mem_do_wdata <= 0;
		end

		if (set_mem_do_rinst)
			mem_do_rinst <= 1;
		if (set_mem_do_rdata)
			mem_do_rdata <= 1;
		if (set_mem_do_wdata)
			mem_do_wdata <= 1;

		irq_pending <= next_irq_pending & ~MASKED_IRQ;

		if (!CATCH_MISALIGN) begin
			if (COMPRESSED_ISA) begin
				reg_pc[0] <= 0;
				reg_next_pc[0] <= 0;
			end else begin
				reg_pc[1:0] <= 0;
				reg_next_pc[1:0] <= 0;
			end
		end
		current_pc = 'bx;
	end



	localparam cpu_state_trap   = 8'b10000000;
	localparam cpu_state_fetch  = 8'b01000000;
	localparam cpu_state_ld_rs1 = 8'b00100000;
	localparam cpu_state_ld_rs2 = 8'b00010000;
	localparam cpu_state_exec   = 8'b00001000;
	localparam cpu_state_shift  = 8'b00000100;
	localparam cpu_state_stmem  = 8'b00000010;
	localparam cpu_state_ldmem  = 8'b00000001;



resetn
    cpu_state <= cpu_state_fetch;
    mem_do_rinst <= 0;

cpu_state = cpu_state_fetch  
    mem_do_rinst <= !decoder_trigger && !do_waitirq;
    assign mem_la_read = resetn && ((!mem_la_use_prefetched_high_word && !mem_state && (mem_do_rinst || mem_do_prefetch || mem_do_rdata)) );
    assign mem_la_addr = (mem_do_prefetch || mem_do_rinst) ? {next_pc[31:2] + mem_la_firstword_xfer, 2'b00} : {reg_op1[31:2], 2'b00};

    if (mem_la_read || mem_la_write) begin
	    mem_addr <= mem_la_addr;























